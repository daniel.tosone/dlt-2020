# Securing the Art Market with Distributed Public Ledgers

This repository contains the source code of the platform developed for the joint work with Marino Miculan named "Securing the Art Market with Distributed Public Ledgers". The work has been presented at DLT 2020, in Ancona on 11/02/2020.

The implementation follows the structure of a *truffle project*. Therefore, the repository includes the following folders and files:
- *build*, contains the files obtained after the compilation of smart contracts, named *artifacts*. These files are needed for deploy and interaction between front-end and back-end
- *contracts*, contains the smart contracts
- *migrations*, contains the scripts used for deployment
- *src*, contains front-end's source code (HTML, CSS, Javascript)
- *truffle-config.js*, is the configuration file for deployment with Truffle
- *bs-config.json*, *package-lock.json*, *package.json* contain information about scripts needed and their execution

Smart contracts source code is also available at their Etherscan pages:
- [ArtistMng](https://ropsten.etherscan.io/address/0xd5ccf599fbb8bca985f4ef5dcfd81374a53f8ac5)
- [Authentication](https://ropsten.etherscan.io/address/0x38f662934ce6c206504c39af82cfd7616a2c6c94)
- [BrokerMng](https://ropsten.etherscan.io/address/0x3328c345d570b66381b7e7de3c0b019c48727e24)
- [Marketplace](https://ropsten.etherscan.io/address/0x4D89583d0eFF1F5d424aab60D20c38e1F6719Ab4)
- [Token](https://ropsten.etherscan.io/token/0x7a5d6593885e5d84acb0ad1c72977294ec961d11)