pragma solidity ^0.5.0;

import "../contracts/openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "./strings.sol";

contract ArtTokenInterface {
    function tokenURI(uint256 tokenId) public view returns (string memory);
    function ownerOf(uint256 tokenId) public view returns (address);
    function getApproved(uint256 tokenId) public view returns (address operator);
    function safeTransferFrom(address from, address to, uint256 tokenId) public;
}

contract BrokerMngInterface {
    function addSoldArtwork(address brokerAddr, uint tokenId, uint sellingPrice, uint commission) external;
    function getSellingPercentage(uint tokenId) external view returns (uint);
}

contract ArtistMngInterface {
    function hasResaleRight(address artistAddr) external view returns (bool);
    function getAuthorized(address artistAddr) external view returns (address payable);
    function getAddressByName(string calldata artistName) external view returns (address payable);
}

contract Marketplace is Ownable {
    using strings for *;

    event ProfessionalPrivateSelling(address indexed seller, uint256 indexed tokenId);
    event Selling(uint256 indexed tokenId, uint256 indexed price);

    ArtTokenInterface artTokenContract;
    BrokerMngInterface brokerMngContract;
    ArtistMngInterface artistMngContract;

    struct Article {
        uint tokenId;
        address payable seller;
        uint price;         // (in wei), viene fatta automaticamente la conversione
        address authBuyer;  // 0x0 se aperta a tutti
    }

    struct Investor {
        address payable addr;
        bool isProfessional;
        string eMail;
    }

    Article[] public onSale;
    mapping (uint => uint) idToIndex; // uso mapping per migliorare efficienza
    Investor[] public investors;
    mapping (address => uint) addressToIndex;
    mapping (address => uint) pendingWithdrawals;
    mapping (uint => uint) public lastSellingPrice; //lo lascio public per trasparenza e per agevolare tutela circolazione opere
    address payable siaeAddress;

//-----INVESTORS------------------------------------------------------------------------------------------------

    modifier onlyInvestors() {
        require(isInvestor(msg.sender), "Only investors can perform this action");
        _;
    }

    function isInvestor(address addr) public view returns (bool){
        uint index = addressToIndex[addr];
        if(investors.length != 0) {
            return (investors[index].addr == addr);
        } else return false;
    }

    function isProfessional(address addr) public view returns (bool){
        uint index = addressToIndex[addr];
        if(investors.length != 0) {
            return (investors[index].addr == addr && investors[index].isProfessional);
        } else return false;
    }

    function addInvestor(address payable investorAddr, bool professional, string memory email) public onlyOwner {
        addressToIndex[investorAddr] = investors.push(Investor(investorAddr, professional, email)) - 1;
    }

    function countInvestors() external view returns(uint){
        return investors.length;
    }

//---------------------------------------------------------------------------------------------------------------------

    function setArtTokenAddr(address addr) external onlyOwner {
        artTokenContract = ArtTokenInterface(addr);
    }

    function setBrokerMngAddr(address addr) external onlyOwner {
        brokerMngContract = BrokerMngInterface(addr);
    }

    function setArtistMngAddr(address addr) external onlyOwner {
        artistMngContract = ArtistMngInterface(addr);
    }

    function setSiaeAddr(address payable addr) external onlyOwner {
        siaeAddress = addr;
    }

    function startOpenSale(uint tokenId, uint price) public {
        require(msg.sender == artTokenContract.ownerOf(tokenId), "You're not authorized to sell this token");
        if(isOnSale(tokenId)){
            removeFromSale(tokenId); // se il quadro è già in vendita allora lo rimuove e ricarica con il prezzo aggiornato
        }
        idToIndex[tokenId] = onSale.push(Article(tokenId, msg.sender, price, address(0x0))) - 1;
    }

    function startClosedSale(uint tokenId, uint price, address authBuyer) public {
        require(msg.sender == artTokenContract.ownerOf(tokenId) ||
                 msg.sender == artTokenContract.getApproved(tokenId), "You're not authorized to sell this token");
        require(isInvestor(authBuyer), "Only investors can buy artworks");
        if(isOnSale(tokenId)){
            removeFromSale(tokenId);
        }
        idToIndex[tokenId] = onSale.push(Article(tokenId, msg.sender, price, authBuyer)) - 1;
    }

    function sellPrivately(uint tokenId, address to) public {
        address owner = artTokenContract.ownerOf(tokenId);
        address seller = msg.sender;
        require(isInvestor(to), "Only investors can buy artworks");
        require(seller == owner || seller == artTokenContract.getApproved(tokenId), "You're not authorized to sell this token");
        artTokenContract.safeTransferFrom(seller, to, tokenId);
        if(seller!=owner) {
            brokerMngContract.addSoldArtwork(seller, tokenId, 0, 0);
            emit ProfessionalPrivateSelling(seller, tokenId);
        } else if(isProfessional(seller) || isProfessional(to)){
            emit ProfessionalPrivateSelling(seller, tokenId);
        } // non serve segnalare le vendite private in cui non interviene un professionista
        // (la tracciabilità è già assicurata nel contratto Token.sol)
    }

    function buy(uint tokenId) public payable {
        require(isInvestor(msg.sender), "Only investors can buy artworks");
        address authBuyer = getAuthBuyer(tokenId);
        uint price = getPrice(tokenId);
        // require ritorna l'importo pagato in caso di fallimento
        require(isOnSale(tokenId) && (msg.sender == authBuyer || authBuyer == address(0x0)), "You're not authorized to buy this artwork");
        require(msg.value == price, "The amount you paid is not enough to buy this artwork");
        address payable owner = address(uint160(artTokenContract.ownerOf(tokenId)));
        address payable seller = getSeller(tokenId);
        removeFromSale(tokenId);
        artTokenContract.safeTransferFrom(owner, msg.sender, tokenId);
        address payable artistAddr = getArtist(tokenId);
        uint resaleAmount = 0;
        if(artistMngContract.hasResaleRight(artistAddr) && owner!=artistAddr){
            resaleAmount = getResaleRight(price);
        }
        if(seller != owner) { // opera in affidamento (intervento professionista => diritto di seguito e commissioni)
            uint percentage = brokerMngContract.getSellingPercentage(tokenId);
            pendingWithdrawals[artistAddr] += resaleAmount;
            owner.transfer((100-percentage) * (price / 100) - resaleAmount);
            seller.transfer(percentage * (price / 100));
            brokerMngContract.addSoldArtwork(seller, tokenId, price, (percentage * (price / 100)));
        } else if (isProfessional(seller) || isProfessional(msg.sender)) { // vendita con professionista => diritto di seguito
            pendingWithdrawals[artistAddr] += resaleAmount;
            seller.transfer(price - resaleAmount);
        } else {
            seller.transfer(price);
        }
        lastSellingPrice[tokenId] = price;
        emit Selling(tokenId, price/1000000000000000000); // il prezzo viene visualizzato in Ether
    }

    function getArtist(uint tokenId) public view returns(address payable){
        strings.slice memory sURI = (artTokenContract.tokenURI(tokenId)).toSlice();
        strings.slice memory sArtist = sURI.split("; ".toSlice());
        string memory artistString = sArtist.toString();
        address payable artist = artistMngContract.getAddressByName(artistString);
        return artist;
    }

    function getResaleRight(uint price) public pure returns (uint){
        uint euroPrice = price/1000000000000000000 * 165; // il diritto di seguito si calcola sul prezzo in euro. Cambio ETH/EUR = 165 (10/09)
        if(euroPrice <= 3000) {
            return 0;
        } else if(euroPrice <= 50000) {
            return price * 4 / 100;
        } else if(euroPrice <= 200000) {
            return price * 3 / 100;
        } else if(euroPrice <= 350000) {
            return price * 1 / 100;
        } else if(euroPrice <= 500000) {
            return price * 5 / 1000;
        } else {
            return min(25 * price / 10000, 12500 * (1000000000000000000 / uint(165)));
        }
    }

    function min(uint a, uint b) private pure returns (uint) {
        return a < b ? a : b;
    }

    function removeFromSale(uint tokenId) private {
        require(isOnSale(tokenId), "This article is not on sale");
        for(uint i = 0; i < onSale.length; i++) {
            if(onSale[i].tokenId == tokenId) {
                onSale[i] = onSale[onSale.length-1];
                delete onSale[onSale.length-1];
                onSale.length--;
                break;
            }
        }
    }

    function isOnSale(uint tokenId) public view returns(bool){
        uint index = idToIndex[tokenId];
        return(onSale.length > 0 && onSale[index].tokenId == tokenId);
    }

    function getPrice(uint tokenId) public view returns(uint) {
        require(isOnSale(tokenId), "Article not on sale");
        uint index = idToIndex[tokenId];
        return onSale[index].price;
    }

    function getSeller(uint tokenId) public view returns(address payable) {
        require(isOnSale(tokenId), "Article not on sale");
        uint index = idToIndex[tokenId];
        return onSale[index].seller;
    }

    function getAuthBuyer(uint tokenId) public view returns(address){
        require(isOnSale(tokenId), "Article not on sale");
        uint index = idToIndex[tokenId];
        return onSale[index].authBuyer;
    }

    function countArticles() public view returns(uint){
        return onSale.length;
    }

    function withdraw(address artistAddr) public {
        require(msg.sender == artistAddr ||
                msg.sender == artistMngContract.getAuthorized(artistAddr) || msg.sender == siaeAddress, "You're not authorized to withdraw this funds");
        uint amount = pendingWithdrawals[artistAddr];
        // Remember to zero the pending refund before
        // sending to prevent re-entrancy attacks
        pendingWithdrawals[artistAddr] = 0;
        msg.sender.transfer(amount);
    }
}