pragma solidity ^0.5.0;

import "./openzeppelin-solidity/contracts/token/ERC721/ERC721Enumerable.sol";
import "./openzeppelin-solidity/contracts/token/ERC721/ERC721MetadataMintable.sol";


contract Token is ERC721Enumerable, ERC721MetadataMintable {

    function getTokensOfOwner(address owner) public view returns (uint[] memory){
        return _tokensOfOwner(owner);
    }

    constructor() public payable
     ERC721Metadata('ArtToken', 'ART') {}

}