pragma solidity ^0.5.0;

import "../contracts/openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract ArtTokenInterface {
    function mintWithTokenURI(address to, uint256 tokenId, string calldata tokenURI) external returns (bool);
    function totalSupply() public view returns (uint256);
}

contract ArtistMng is Ownable {

    struct Artist {
        address addr;
        string  name;   // and surname
        uint[]  producedArtworks;
        string eMail;
        bool resaleRight;
        address payable authorized;
    }

    Artist[] public artists;
    mapping (address=>uint) public addressToIndex; // mapping per migliorare efficienza nella ricerca
    mapping (string=>address payable) public nameToAddress;
    ArtTokenInterface artTokenContract;

    modifier onlyArtists() {
        require(isArtist(msg.sender), "Only artists can perform this action");
        _;
    }

    function setArtTokenAddr(address addr) external onlyOwner {
        artTokenContract = ArtTokenInterface(addr);
    }

    function setAuthorized(address payable addr) external {
        uint index = addressToIndex[msg.sender];
        artists[index].authorized = addr;
    }

    function getAuthorized(address addr) external view returns (address payable){
        require(isArtist(addr), "No artist with that address");
        uint index = addressToIndex[addr];
        return artists[index].authorized;
    }

    function getNameByAddress(address addr) public view returns (string memory){
        require(isArtist(addr), "No artist with that address");
        uint index = addressToIndex[addr];
        return artists[index].name;
    }

    function getAddressByName(string calldata artistName) external view returns (address payable) {
        return nameToAddress[artistName];
    }

    function hasResaleRight(address addr) external view returns (bool) {
        uint index = addressToIndex[addr];
        return artists[index].resaleRight;
    }

    function setResaleRight(address addr, bool hasRight) external onlyOwner {
        uint index = addressToIndex[addr];
        artists[index].resaleRight = hasRight;
    }

    function countArtists() external view returns (uint){ // non è possibile ritornare array di Struct se non in versioni sperimentali
        return artists.length;
    }

    function isArtist(address addr) public view returns (bool){
        uint index = addressToIndex[addr];
        if(artists.length != 0) {
            return (artists[index].addr == addr);
        } else return false;
        // indirizzi non registrati come artisti restituiscono 0 come indice quindi devo verificare che coincida
    }

    function addArtist(address payable artistAddr, string calldata artistName, string calldata eMail) external onlyOwner {
        addressToIndex[artistAddr] = artists.push(Artist(artistAddr, artistName, new uint[](0), eMail, true, address(0x0))) - 1;
        // array delle opere prodotte vuoto
        nameToAddress[artistName] = artistAddr;
    }

    function registerArtwork(
        string calldata title,
        string calldata year,
        string calldata medium,
        string calldata dimensions,
        string calldata hash
        ) external onlyArtists {

        string memory artistName = getNameByAddress(msg.sender);
        // versione gas-efficient del string append
        string memory artworkDetails = string(abi.encodePacked(
            artistName, "; ",
            title, "; ",
            year, "; ",
            medium, "; ",
            dimensions, "; ",
            artistName, "; ",
            hash));

        uint tokenId = artTokenContract.totalSupply();
        artTokenContract.mintWithTokenURI(msg.sender, tokenId, artworkDetails);
        addProducedArtwork(msg.sender, tokenId);
    }

    function addProducedArtwork(address artistAddr, uint tokenId) private {
        uint index = addressToIndex[artistAddr];
        require(artists[index].addr == artistAddr, "No artist with that address");
        artists[index].producedArtworks.push(tokenId);
    }

    // chiedo direttamente l'indice in modo da migliorare l'efficienza per gli artisti
    //        index = addressToIndex[artistAddr];
    function getProducedArtworks(uint index) external view returns(uint[] memory){
        return artists[index].producedArtworks;
    }

}