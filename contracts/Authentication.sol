pragma solidity ^0.5.0;

import "../contracts/openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract ArtTokenInterface {
    function mintWithTokenURI(address to, uint256 tokenId, string calldata tokenURI) external returns (bool);
    function totalSupply() public view returns (uint256);
}

contract Authentication is Ownable {

    struct Authenticator {
        address addr;
        string  name;   // the identity of the authenticator
        uint[]  authArtworks;
        string eMail;
    }

    struct Request {
        string hash;
        address proposer;   // solo chi ha la foto dell'opera può proporre e registrare un'opera
    }

    struct Proposal {
        string artist;
        string title;
        string year;
        string medium;
        string dimensions;
        address[] authenticators;
    }

    Authenticator[] public authenticators;
    Request[] public toAuth;
    mapping (string=>Proposal[]) public proposals;
    mapping (string=>mapping(address=>bool)) public hasVoted;
    uint public requiredAuth = 2;
    mapping (address=>uint) public addressToIndex;
    ArtTokenInterface artTokenContract;

    modifier onlyAuthenticators() {
        require(isAuthenticator(msg.sender), "Only authenticators can perform this action");
        _;
    }

    function setArtTokenAddr(address addr) external onlyOwner {
        artTokenContract = ArtTokenInterface(addr);
    }

    function getNameByAddress(address addr) public view returns (string memory){
        require(isAuthenticator(addr), "No authenticator with that address");
        uint index = addressToIndex[addr];
        return authenticators[index].name;
    }

    // Solidity non permette di ritornare array di Struct se non in versioni sperimentali
    function countAuthenticators() external view returns (uint){
        return authenticators.length;
    }

    function countToAuthenticate() external view returns (uint){
        return toAuth.length;
    }

    // calldata è analogo a memory ma un po' più efficiente
    function countProposals(string calldata hash) external view returns (uint){
        return proposals[hash].length;
    }

    function isAuthenticator(address addr) public view returns (bool){
        uint index = addressToIndex[addr];
        if(authenticators.length != 0) {
            return (authenticators[index].addr == addr);
        } else return false;
        // indirizzi non registrati come authenticators restituiscono 0 come indice quindi devo verificare che coincida
    }

    //l'unico che può aggiungere un authenticator è l'amministratore (chi rilascia il contratto)
    function addAuthenticator(address authAddr, string calldata authName, string calldata eMail) external onlyOwner {
        addressToIndex[authAddr] = authenticators.push(Authenticator(authAddr, authName, new uint[](0), eMail)) - 1;
    }

    // viene richiesta l'autenticazione. NON viene creato il token in questa fase.
    // Servono requiredAuth autenticazioni per creare il token
    // chiunque sia in possesso dell'opera può chiederne l'autenticazione
    function askForAuthentication(string calldata hash) external {
        toAuth.push(Request(hash, msg.sender));
    }

    // @param hash -> hash del quadro che voglio autenticare
    // @param index -> indice della proposta che voglio sottoscrivere
    function authenticate(string calldata hash, uint index) external onlyAuthenticators {
        require(!hasVoted[hash][msg.sender], "You cannot vote twice, only read available");
        proposals[hash][index].authenticators.push(msg.sender);
        hasVoted[hash][msg.sender] = true;
    }

    function addProposal(
        string calldata hash,
        string calldata artist,
        string calldata title,
        string calldata year,
        string calldata medium,
        string calldata dimensions
    ) external onlyAuthenticators {
        require(!hasVoted[hash][msg.sender], "You cannot vote twice, only read available");
        uint index = proposals[hash].push(Proposal(artist, title, year, medium, dimensions, new address[](0))) - 1;
        proposals[hash][index].authenticators.push(msg.sender);
        hasVoted[hash][msg.sender] = true;
    }

    // hash del quadro da registrare e indice della proposta vincitrice
    function registerAuthenticatedArtwork(string calldata hash, uint index) external {
        address[] memory authentications = getAuthentications(hash, index);
        address proposer;
        uint toAuthIndex;
        uint length = toAuth.length;
        for(uint i = 0; i<length; i++){
            if(compareStrings(toAuth[i].hash, hash)) {
                proposer = toAuth[i].proposer;
                toAuthIndex = i;
                break;
            }
        }
        require(authentications.length >= requiredAuth, "You need requiredAuth authentications to register this artwork");

        Proposal memory winningProposal = proposals[hash][index];
        string memory artworkDetails = string(abi.encodePacked(
            winningProposal.artist, "; ",
            winningProposal.title, "; ",
            winningProposal.year, "; ",
            winningProposal.medium, "; ",
            winningProposal.dimensions, "; ",
            stringify(winningProposal.authenticators), "; ",
            hash));

        uint tokenId = artTokenContract.totalSupply();
        artTokenContract.mintWithTokenURI(proposer, tokenId, artworkDetails);
        for(uint i = 0; i<winningProposal.authenticators.length; i++) {
            addAuthArtwork(winningProposal.authenticators[i], tokenId);
            // aggiungo alla lista delle opere autenticate solo quelle che vengono effettivamente registrate.
        }
        //rimuovo richiesta e proposte
        toAuth[toAuthIndex] = toAuth[toAuth.length - 1];
        toAuth.length--; // delete implicito dell'ultima posizione
        clearHasVoted(hash);
        delete proposals[hash];
    }

    //aggiunge l'opera alle opere autenticate dall'indirizzo
    function addAuthArtwork(address authAddr, uint tokenId) private {
        uint index = addressToIndex[authAddr];
        if(authenticators[index].addr == authAddr) {
            authenticators[index].authArtworks.push(tokenId);
        }
    }

    function clearHasVoted(string memory hash) private {
        Proposal[] memory proposalsList = proposals[hash];
        for(uint i = 0; i<proposalsList.length; i++) {
            address[] memory authenticatorsList = proposalsList[i].authenticators;
            for(uint j = 0; j<authenticatorsList.length; j++){
                delete hasVoted[hash][authenticatorsList[i]];
            }
        }
    }

    // chiedo direttamente l'indice in modo da migliorare l'efficienza per gli authenticatori
    // (nel front-end verrà tenuto in memoria l'indice)
    //        index = addressToIndex[authenticatorAddr];
    function getAuthArtworks(uint index) external view returns(uint[] memory){
        return authenticators[index].authArtworks;
    }

    function getAuthentications(string memory hash, uint index) public view returns(address[] memory){
       return proposals[hash][index].authenticators;
    }

    function compareStrings (string memory a, string memory b) private pure returns (bool) {
        return (keccak256(abi.encodePacked((a))) == keccak256(abi.encodePacked((b))));
    }

    function stringify(address[] memory array) private view returns (string memory) {
        string memory result = getNameByAddress(array[0]);
        for(uint i = 1; i<array.length; i++) {
            result = string(abi.encodePacked(result, ", ", getNameByAddress(array[i])));
        }
        return result;
    }
/*
    function addressToString(address _addr) public pure returns(string memory) {
        bytes32 value = bytes32(uint256(_addr));
        bytes memory alphabet = "0123456789abcdef";

        bytes memory str = new bytes(42);
        str[0] = '0';
        str[1] = 'x';
        for (uint i = 0; i < 20; i++) {
            str[2+i*2] = alphabet[uint(uint8(value[i + 12] >> 4))];
            str[3+i*2] = alphabet[uint(uint8(value[i + 12] & 0x0f))];
        }
        return string(str);
    }
*/
}