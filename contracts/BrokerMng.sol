pragma solidity ^0.5.0;

import "../contracts/openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract ArtTokenInterface {
    function ownerOf(uint256 tokenId) public view returns (address);
    function approve(address to, uint256 tokenId) public;
}

contract BrokerMng is Ownable {

    struct Broker {
        address addr;
        string  name;
        uint[]  entrustedArtworks;
        string email;
    }

    struct Sold {
        address requester;
        uint tokenId;
        uint sellingPrice;
        uint commission;
        // sellingPrice e commission in wei. Disponibili solo per vendite su Ethereum
    }

    Broker[] public brokers;
    mapping (address=>Sold[]) public soldArtworks; // deve stare fuori dalla struct per esigenze d'interazione con il front-end
    mapping (uint=>address) public currentRequester;
    mapping (address=>uint) public addressToIndex; // uso map per migliorare efficienza nella ricerca
    mapping (uint=>uint) private sellingPercentage; // percentuale che spetta alla vendita (intero tra 0 e 100)
    address marketAddress;
    ArtTokenInterface artTokenContract;

    function setArtTokenAddr(address addr) external onlyOwner {
        artTokenContract = ArtTokenInterface(addr);
    }

    function setMarketAddr(address addr) external onlyOwner {
        marketAddress = addr;
    }

    function getNameByAddress(address addr) external view returns (string memory){
        require(isBroker(addr), "No broker with that address");
        uint index = addressToIndex[addr];
        return brokers[index].name;
    }

    function countBrokers() external view returns (uint){ // non è possibile ritornare array di Struct se non in versioni sperimentali
        return brokers.length;
    }

    function isBroker(address addr) public view returns (bool){
        uint index = addressToIndex[addr];
        if(brokers.length != 0) {
            return (brokers[index].addr == addr);
        } else return false;
        // indirizzi non registrati come intermediari restituiscono 0 come indice quindi devo verificare che coincida
    }

    //l'unico che può aggiungere un broker è l'amministratore (chi rilascia il contratto)
    function addBroker(address brokerAddr, string calldata brokerName, string calldata eMail) external onlyOwner {
        addressToIndex[brokerAddr] = brokers.push(Broker(brokerAddr, brokerName, new uint[](0), eMail)) - 1;
    }

    function entrustArtwork(address brokerAddr, uint tokenId, uint percentage) external {
        address owner = artTokenContract.ownerOf(tokenId);
        require(brokerAddr != owner, "Approval to current owner");
        require(msg.sender == owner, "Approve caller is not owner");
        require(isBroker(brokerAddr), "Approval to a non-broker");

        artTokenContract.approve(brokerAddr, tokenId);

        uint index = addressToIndex[brokerAddr];
        brokers[index].entrustedArtworks.push(tokenId);
        sellingPercentage[tokenId] = percentage;
        currentRequester[tokenId] = owner;
    }

    // chiedo direttamente l'indice in modo da migliorare l'efficienza per gli intermediari
    //        index = addressToIndex[brokerAddr];
    function getEntrustedArtworks(uint index) external view returns(uint[] memory){
        return brokers[index].entrustedArtworks;
    }

    // funzione chiamata in automatico da addSoldArtwork
    function removeEntrustedArtwork(address brokerAddr, uint tokenId) private {
        uint index = addressToIndex[brokerAddr];
        uint[] storage entrustedArtworks = brokers[index].entrustedArtworks;
        for(uint i = 0; i<entrustedArtworks.length; i++) {
            if(entrustedArtworks[i] == tokenId) {
                entrustedArtworks[i] = entrustedArtworks[entrustedArtworks.length-1];
                delete entrustedArtworks[entrustedArtworks.length-1];
                entrustedArtworks.length--;
                break;
            }
        }
    }

    // funzione chiamata in automatico da marketplace al momento della vendita
    function addSoldArtwork(address brokerAddr, uint tokenId, uint sellingPrice, uint commission) external {
        require(msg.sender == marketAddress, "You cannot perform this action");
        soldArtworks[brokerAddr].push(Sold(currentRequester[tokenId], tokenId, sellingPrice, commission));
        delete sellingPercentage[tokenId];
        delete currentRequester[tokenId];
        removeEntrustedArtwork(brokerAddr, tokenId);
    }

    // chiedo direttamente l'indice in modo da migliorare l'efficienza per i broker
    //        index = addressToIndex[brokerAddr];
    function countSoldArtworks(address brokerAddr) external view returns(uint){
        return soldArtworks[brokerAddr].length;
    }

    function getSellingPercentage(uint tokenId) external view returns (uint){
        return sellingPercentage[tokenId];
    }

}