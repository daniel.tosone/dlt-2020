AdmApp = {
    tokenInstance: null,
    artistMngInstance: null,
    brokerMngInstance: null,
    authInstance: null,
    marketplaceInstance: null,
    web3Provider: null,
    contracts: {},
    account: '0x0',
  
    init: function() {
      return this.initWeb3();
    },
  
    initWeb3: function() {
      // se metamask è attivo, viene visto solo l'indirizzo selezionato (array di un elemento)
      // in futuro sarebbe meglio eliminare metamask ma bisogna predisporre la firma delle transazioni in backend
      if (typeof web3 !== 'undefined') {
        // If a web3 instance is already provided by Meta Mask.
        this.web3Provider = web3.currentProvider;
        web3 = new Web3(web3.currentProvider);
      } else {
        // Specify default instance if no web3 instance provided
        this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        web3 = new Web3(this.web3Provider);
      }
      return this.initContracts();
    },
  
    initContracts: async function() {
      await $.getJSON("Token.json", function(token) {
        // Instantiate a new truffle contract from the artifact
        AdmApp.contracts.Token = TruffleContract(token);
        // Connect provider to interact with contract
        AdmApp.contracts.Token.setProvider(AdmApp.web3Provider);
      });
  
      await $.getJSON("ArtistMng.json", function(artistMng) {
        AdmApp.contracts.ArtistMng = TruffleContract(artistMng);
        AdmApp.contracts.ArtistMng.setProvider(AdmApp.web3Provider);
      });

      await $.getJSON("BrokerMng.json", function(brokerMng) {
        AdmApp.contracts.BrokerMng = TruffleContract(brokerMng);
        AdmApp.contracts.BrokerMng.setProvider(AdmApp.web3Provider);
      });

      await $.getJSON("Marketplace.json", function(marketplace) {
        AdmApp.contracts.Marketplace = TruffleContract(marketplace);
        AdmApp.contracts.Marketplace.setProvider(AdmApp.web3Provider);
      });
      
      await $.getJSON("Authentication.json", function(authentication) {
        AdmApp.contracts.Authentication = TruffleContract(authentication);
        AdmApp.contracts.Authentication.setProvider(AdmApp.web3Provider);
      });

      return this.initFields();
    },
  
    initFields: async function(){
  
      // Load account data
      this.account = await web3.eth.accounts[0]; 
      $("#accountAddress").html("Your account:" + this.account);
  
      // Load contracts data
      this.tokenInstance = await this.contracts.Token.deployed();
      this.artistMngInstance = await this.contracts.ArtistMng.deployed();
      this.brokerMngInstance = await this.contracts.BrokerMng.deployed();
      this.marketplaceInstance = await this.contracts.Marketplace.deployed();
      this.authInstance = await this.contracts.Authentication.deployed();
      return this.render();
    },
    
    render: function() {
      var loader = $("#loader");
      var content = $("#content");
      var artists = $("#artists");
      var authenticators = $("#authenticators");
      var brokers = $("#brokers");
      var investors = $("#investors");      

      
      loader.hide();
      artists.hide();
      authenticators.hide();
      brokers.hide();
      investors.hide();
      content.show();
    },

    quickstart: async function() {

      // TOKEN
      this.tokenInstance.setBrokerMngAddr(this.brokerMngInstance.address);
      this.tokenInstance.setMarketAddr(this.marketplaceInstance.address);
      await this.tokenInstance.addMinter(this.artistMngInstance.address);
      await this.tokenInstance.addMinter(this.authInstance.address);
      this.tokenInstance.renounceMinter();

      // ARTISTMNG
      this.artistMngInstance.setArtTokenAddr(this.tokenInstance.address);

      // BROKERMNG 
      this.brokerMngInstance.setArtTokenAddr(this.tokenInstance.address);
      this.brokerMngInstance.setMarketAddr(this.marketplaceInstance.address);

      // AUTHENTICATION
      this.authInstance.setArtTokenAddr(this.tokenInstance.address);

      // MARKETPLACE
      this.marketplaceInstance.setArtTokenAddr(this.tokenInstance.address);
      this.marketplaceInstance.setBrokerMngAddr(this.brokerMngInstance.address);
      this.marketplaceInstance.setArtistMngAddr(this.artistMngInstance.address);

    },

    addArtist: function() {
      var form = document.getElementById("addArtist");
        
      var address1 = form[0].value;
      var name1 = form[1].value;
      var email1 = form[2].value;
      
      if(address1!=="" && name1!=="") {
          this.artistMngInstance.addArtist(address1, name1, email1);
      }

    },

    addBroker: function() {
      var form = document.getElementById("addBroker");
      this.brokerMngInstance.addBroker(form[0].value, form[1].value, form[2].value);
    },

    addAuthenticator: function() {
      var form = document.getElementById("addAuthenticator");
      this.authInstance.addAuthenticator(form[0].value, form[1].value, form[2].value);
    },

    addInvestor: function() {
      var form = document.getElementById("addInvestor");
      this.marketplaceInstance.addInvestor(form[0].value, form[3].checked, form[2].value);
    },

    removeResaleRight: function() {
      var artistAddr = document.getElementById("artistResaleRight").value;
      this.artistMngInstance.setResaleRight(artistAddr, false);
    }

  };

  $(function() {
    $(window).load(function() {
      AdmApp.init();
    });
  });