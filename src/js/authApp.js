AuthApp = {
    authInstance: null,
    web3Provider: null,
    contracts: {},
    account: '0x0',
    myIndex: 0,
 
    init: function() {
      return this.initWeb3();
    },
  
    initWeb3: function() {
      // se metamask è attivo, viene visto solo l'indirizzo selezionato (array di un elemento)
      // in futuro sarebbe meglio eliminare metamask ma bisogna predisporre la firma delle transazioni in backend
      if (typeof web3 !== 'undefined') {
        // If a web3 instance is already provided by Meta Mask.
        this.web3Provider = web3.currentProvider;
        web3 = new Web3(web3.currentProvider);
      } else {
        // Specify default instance if no web3 instance provided
        this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        web3 = new Web3(this.web3Provider);
      }
      return this.initContracts();
    },
  
    initContracts: async function() {
  
      await $.getJSON("Authentication.json", function(authentication) {
        AuthApp.contracts.Authentication = TruffleContract(authentication);
        AuthApp.contracts.Authentication.setProvider(AuthApp.web3Provider);
      });
  
      return this.initFields();
    },
  
    initFields: async function(){
  
      // Load account data
      this.account = await web3.eth.accounts[0]; 
      $("#accountAddress").html("Your account:" + this.account);
  
      // Load contracts data
      this.authInstance = await this.contracts.Authentication.deployed();
      myIndex = await this.authInstance.addressToIndex(this.account);
      return this.render();
    },
  
    render: function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworksAuthenticated = $("#artworks");
      var artworksToAuthenticate = $("#artworksToAuthenticate");      
      var artists = $("#artists");
      var authenticators = $("#authenticators");
      var brokers = $("#brokers");
      var investors = $("#investors");      
  
      loader.hide();
      artworksAuthenticated.hide();
      artworksToAuthenticate.hide();
      artists.hide();
      authenticators.hide();
      brokers.hide();
      investors.hide();
      content.show();
    },
  
    showAuthenticatedArtworks: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworksDiv = $("#artworks");
  
      var authenticator = await this.authInstance.authenticators(myIndex);
      var artworks;
      if(authenticator[0] === this.account){
        artworks = await this.authInstance.getAuthArtworks(myIndex);
      } else { // in caso di rimozione di un autenticatore
        myIndex = await this.authInstance.addressToIndex(this.account);
        artworks = await this.authInstance.getAuthArtworks(myIndex);
      }
  
      document.getElementById("titleArtworks").innerText = "Artworks you authenticated";
      MemberApp.displayArtworks(artworks);
  
      var authenticatedButton = document.getElementById("authenticatedButton");
      authenticatedButton.value = "Hide authenticated artworks";
      authenticatedButton.setAttribute( "onClick", "javascript: AuthApp.hideAuthenticatedArtworks();" );
      artworksDiv.show();
      loader.hide();
      content.show();
    },

    hideAuthenticatedArtworks: function() {
  
      var artworksDiv = $("#artworks");
      var loader = $("#loader");
      var content = $("#content");
  
      var authenticatedButton = document.getElementById("authenticatedButton");
      authenticatedButton.value = "Show authenticated artworks";
      authenticatedButton.setAttribute( "onClick", "javascript: AuthApp.showAuthenticatedArtworks();" ); // fare il semplice onclick non funziona
      artworksDiv.hide();
      loader.hide();
      content.show();
    },

    showArtworksToAuthenticate: async function(){
      var loader = $("#loader");
      var content = $("#content");
      var artworks = $("#artworksToAuthenticate");
      var artworksList = $("#toAuthenticateList");

      artworksList.empty();
      var count = await this.authInstance.countToAuthenticate();
      for(var i=0; i<count; i++){
        var artwork = await this.authInstance.toAuth(i);
        var hash = artwork[0];
        var proposer = artwork[1];

        var tr = document.createElement('tr');
        var td = document.createElement('td');

        var button1 = document.createElement('button');
        button1.innerText = "Show";
        button1.className = "btn btn-primary";
        button1.onclick = function(){
          MemberApp.retrieve(hash);
        };
        td.appendChild(button1);
        tr.appendChild(td);

        var button2 = document.createElement('button');
        button2.innerText = "Show";
        button2.className = "btn btn-primary";
        button2.onclick = function() {
          AuthApp.showProposals(hash);
          $('#modalProposals').modal('show');
        };
        td = document.createElement('td'); // sovrascrivo il puntatore alla cella precedente
        td.appendChild(button2);
        tr.appendChild(td);

        var td = document.createElement('td');
        td.innerText = proposer;
        tr.appendChild(td);

        artworksList.append(tr);
      }

      var toAuthenticateButton = document.getElementById("toAuthenticateButton");
      toAuthenticateButton.setAttribute( "onClick", "javascript: AuthApp.hideArtworksToAuthenticate();" );
      toAuthenticateButton.value = "Hide artworks to authenticate";

      artworks.show();
      loader.hide();
      content.show();
    },

    showProposals: async function(hash) {

      $("#addProposalButton")[0].onclick = function() {
        AuthApp.addProposal(hash);
      }

      $("#confirmProposal")[0].onclick = function() {
        AuthApp.confirmProposal(hash);
      }

      $("#confirmSelection")[0].onclick = function() {
        AuthApp.confirmSelection(hash);
      }

      var modalBody = $("#proposalsList");
      modalBody.empty();
      var count = await this.authInstance.countProposals(hash);
      for(i = 0; i<count; i++) {
        modalBody.append("<br><input type='radio' name='proposals' value='" + i + "'>");
        var proposal = await this.authInstance.proposals(hash, i);
        for(j = 0; j<4; j++) {
          modalBody.append(" " + proposal[j] + "; ");
        }
        modalBody.append(proposal[j]);
        modalBody.append("<br>");
        var authentications = await this.authInstance.getAuthentications(hash, i);
        modalBody.append("Conferme: <b>" + authentications.length  + "</b><br><br>");
      }
    },

    addProposal: async function(hash) {
      if(await this.authInstance.hasVoted(hash, this.account)){
        alert("You cannot vote proposals twice, only read available");
        return;
      }
      $('#addProposal').show();
    },

    confirmProposal: function(hash) {
      var div = $("#addProposal")[0];
      var artist = div.children[0].value;
      var title = div.children[2].value;
      var year = div.children[4].value;
      var medium = div.children[6].value;
      var dimensions = div.children[8].value;
      // devo saltare uno perchè i <br> contano come figli

      if(artist !== "" && title !== "" && year !== "" && medium !== "" && dimensions !== ""){
        $("#addProposal").hide();
        this.authInstance.addProposal(hash, artist, title, year, medium, dimensions); // da gestire con un then
//        console.log("Aggiunta avvenuta con successo. Il voto è già stato registrato");
      }
      $('#modalProposals').modal('hide');
    }, 

    hideArtworksToAuthenticate: function() {
  
      var artworksDiv = $("#artworksToAuthenticate");
      var loader = $("#loader");
      var content = $("#content");
  
      var toAuthenticateButton = document.getElementById("toAuthenticateButton");
      toAuthenticateButton.value = "Show artworks to authenticate";
      toAuthenticateButton.setAttribute( "onClick", "javascript: AuthApp.showArtworksToAuthenticate();" );
      artworksDiv.hide();
      loader.hide();
      content.show();
    },
  
    confirmSelection: async function(hash) {
      if(await this.authInstance.hasVoted(hash, this.account)){
        alert("You cannot vote proposals twice, only read available");
        return;
      }      
      var radiovalue = $("input[name='proposals']:checked").val();
      this.authInstance.authenticate(hash, radiovalue);
      $('#modalProposals').modal('hide');
    },

  };
  
  $(function() {
    $(window).load(function() {
      AuthApp.init();
    });
  });
