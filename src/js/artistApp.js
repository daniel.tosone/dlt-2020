ArtistApp = {
  artistMngInstance: null,
  tokenInstance: null,
  web3Provider: null,
  contracts: {},
  account: '0x0',
  myIndex: 0,

  init: function() {
    return this.initWeb3();
  },

  initWeb3: function() {
    // se metamask è attivo, viene visto solo l'indirizzo selezionato (array di un elemento)
    // in futuro sarebbe meglio eliminare metamask ma bisogna predisporre la firma delle transazioni in backend
    if (typeof web3 !== 'undefined') {
      // If a web3 instance is already provided by Meta Mask.
      this.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // Specify default instance if no web3 instance provided
      this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
      web3 = new Web3(this.web3Provider);
    }
    return this.initContracts();
  },

  initContracts: async function() {

    await $.getJSON("ArtistMng.json", function(artistMng) {
      ArtistApp.contracts.ArtistMng = TruffleContract(artistMng);
      ArtistApp.contracts.ArtistMng.setProvider(ArtistApp.web3Provider);
    });

    await $.getJSON("Token.json", function(token) {
      ArtistApp.contracts.Token = TruffleContract(token);
      ArtistApp.contracts.Token.setProvider(ArtistApp.web3Provider);
    });

    return this.initFields();
  },

  initFields: async function(){

    // Load account data
    this.account = await web3.eth.accounts[0]; 
    $("#accountAddress").html("Your account:" + this.account);

    // Load contracts data
    this.artistMngInstance = await this.contracts.ArtistMng.deployed();
    this.tokenInstance = await this.contracts.Token.deployed();

    myIndex = await this.artistMngInstance.addressToIndex(this.account);
    return this.render();
  },

  render: function() {
    var loader = $("#loader");
    var content = $("#content");
    var artworks = $("#artworks");
    var articles = $("#articles");
    var artists = $("#artists");
    var authenticators = $("#authenticators");
    var brokers = $("#brokers");
    var investors = $("#investors");      

    loader.hide();
    artworks.hide();
    articles.hide();
    artists.hide();
    authenticators.hide();
    brokers.hide();
    investors.hide();
    content.show();
  },

  showMyProducedArtworks: async function() {
    var loader = $("#loader");
    var content = $("#content");
    var artworksDiv = $("#artworks");

    var artist = await this.artistMngInstance.artists(myIndex);
    var artworks;
    if(artist[0] === this.account){
      artworks = await this.artistMngInstance.getProducedArtworks(myIndex);
    } else { // in caso di rimozione di un artista
      myIndex = await this.artistMngInstance.addressToIndex(this.account);
      artworks = await this.artistMngInstance.getProducedArtworks(myIndex);
    }

    MemberApp.displayArtworks(artworks);

    document.getElementById("titleArtworks").innerText = "Artworks you produced";
    var producedButton = document.getElementById("producedButton");
    producedButton.value = "Hide produced artworks";
    producedButton.setAttribute( "onClick", "javascript: ArtistApp.hideMyProducedArtworks();" );
    artworksDiv.show();
    loader.hide();
    content.show();
  },

  hideMyProducedArtworks: function() {

    var artworksDiv = $("#artworks");
    var loader = $("#loader");
    var content = $("#content");

    var producedButton = document.getElementById("producedButton");
    producedButton.value = "Show produced artworks";
    producedButton.setAttribute( "onClick", "javascript: ArtistApp.showMyProducedArtworks();" ); // fare il semplice onclick non funziona
    artworksDiv.hide();
    loader.hide();
    content.show();
  },

  registerArtwork: async function() {

    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    var form = document.getElementById("registerArtwork");
    var title = form[0].value;
    var year = form[1].value;
    var medium = form[2].value;
    var dimensions = form[3].value;
    if(form[4].files.length > 0) {
      var file = form[4].files[0];
    } else {
      alert("Image of the artwork required");
      return;
    }

    if(title === "" || year === "" || medium === "" || dimensions === "") {
      alert("Missing fields");
      return;
    }

    var hash = await loadImage(file); // non è possibile calcolare la hash senza aggiungere il quadro su IPFS
    console.log(hash);
    console.log("Image loaded successfully")

    var artworks = await this.artistMngInstance.getProducedArtworks(myIndex); // per motivi di efficienza richedo direttamente l'indice nell'array degli artisti
    for(let id of artworks) {
      var registeredURI = await ArtistApp.tokenInstance.tokenURI(id);
      var registeredDetails = registeredURI.split("; ");
      if(registeredDetails[6] === hash || registeredDetails[1] === title){ //non sono consentiti due quadri con la stessa immagine nè due quadri dello stesso autore con lo stesso titolo (ci dev'essere almeno un indice progressivo)
        alert("Non puoi caricare lo stesso quadro due volte!");
        return;
      }
    }

    ArtistApp.artistMngInstance.registerArtwork(title, year, medium, dimensions, hash); // da gestire con un then
//    $("#accountAddress").html("Creato il token per \"" + title + "\" con successo");
  },

  setAuthAddress: function() {
    var address = document.getElementById("setAuthAddress")[0].value;
    this.artistMngInstance.setAuthorized(address);
  }

};

$(function() {
  $(window).load(function() {
    ArtistApp.init();
  });
});