InvestorApp = {
    tokenInstance: null,
    brokerMngInstance: null,
    marketplaceInstance: null,
    authInstance: null,
    web3Provider: null,
    contracts: {},
    account: '0x0',
  
    init: function() {
      return this.initWeb3();
    },
  
    initWeb3: function() {
      // se metamask è attivo, viene visto solo l'indirizzo selezionato (array di un elemento)
      // in futuro sarebbe meglio eliminare metamask ma bisogna predisporre la firma delle transazioni in backend
      if (typeof web3 !== 'undefined') {
        // If a web3 instance is already provided by Meta Mask.
        this.web3Provider = web3.currentProvider;
        web3 = new Web3(web3.currentProvider);
      } else {
        // Specify default instance if no web3 instance provided
        this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        web3 = new Web3(this.web3Provider);
      }
      return this.initContracts();
    },
  
    initContracts: async function() {
      await $.getJSON("Token.json", function(token) {
        // Instantiate a new truffle contract from the artifact
        InvestorApp.contracts.Token = TruffleContract(token);
        // Connect provider to interact with contract
        InvestorApp.contracts.Token.setProvider(InvestorApp.web3Provider);
      });
  
      await $.getJSON("BrokerMng.json", function(brokerMng) {
        InvestorApp.contracts.BrokerMng = TruffleContract(brokerMng);
        InvestorApp.contracts.BrokerMng.setProvider(InvestorApp.web3Provider);
      });

      await $.getJSON("Marketplace.json", function(marketplace) {
        InvestorApp.contracts.Marketplace = TruffleContract(marketplace);
        InvestorApp.contracts.Marketplace.setProvider(InvestorApp.web3Provider);
      });

      await $.getJSON("Authentication.json", function(authentication) {
        InvestorApp.contracts.Authentication = TruffleContract(authentication);
        InvestorApp.contracts.Authentication.setProvider(InvestorApp.web3Provider);
      });


      return this.initFields();
    },
  
    initFields: async function(){
  
      // Load account data
      this.account = await web3.eth.accounts[0]; 
      $("#accountAddress").html("Your account:" + this.account);
  
      // Load contracts data
      this.tokenInstance = await this.contracts.Token.deployed();
      this.brokerMngInstance = await this.contracts.BrokerMng.deployed();
      this.marketplaceInstance = await this.contracts.Marketplace.deployed();
      this.authInstance = await this.contracts.Authentication.deployed();
  
      return this.render();
    },
    
    render: function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworksDiv = $("#artworks");
      var toAuthenticate = $("#artworksToAuthenticate")
      var articles = $("#articles");
      var artists = $("#artists");
      var authenticators = $("#authenticators");
      var brokers = $("#brokers");
      var investors = $("#investors");      
  
      loader.hide();
      artworksDiv.hide();
      toAuthenticate.hide();
      articles.hide();
      artists.hide();
      authenticators.hide();
      brokers.hide();
      investors.hide();
      content.show();
    },
        
    showArtworksOnSale: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var articles = $("#articles");
      var articlesList = $("#articlesList");

      articlesList.empty();
      var count = await this.marketplaceInstance.countArticles();
      for(var i=0; i<count; i++){
        var article = await this.marketplaceInstance.onSale(i);
        var tokenId = article[0];
        var price = article[2];
        var authBuyer = article[3];
        if(authBuyer == '0x0000000000000000000000000000000000000000' || authBuyer == this.account) {
          var details = (await InvestorApp.tokenInstance.tokenURI(tokenId)).split("; ");
          var tr = document.createElement('tr');
          var th = document.createElement('th');
          th.innerText = tokenId;
          tr.appendChild(th);

          var td = document.createElement('td');
          td.innerText = price/1000000000000000000;
          tr.appendChild(td);

          for(var j = 0; j<6; j++){
            var td = document.createElement('td');
            td.innerText = details[j];
            tr.appendChild(td);
          }
          var button = MemberApp.createButton(details[6]); // se non uso forEach i pulsanti puntano tutti alla stessa hash. ASSURDO! Con un metodo extra il problema si risolve
          tr.appendChild(button);
          articlesList.append(tr);
  
        }
      }

      var onSaleButton = document.getElementById("onSaleButton");
      onSaleButton.setAttribute( "onClick", "javascript: InvestorApp.hideArtworksOnSale();" );
      onSaleButton.value = "Hide artworks on sale";

      articles.show();
      loader.hide();
      content.show();
    },

    hideArtworksOnSale: function() {
  
      var articles = $("#articles");
      var loader = $("#loader");
      var content = $("#content");
  
      var onSaleButton = document.getElementById("onSaleButton");
      onSaleButton.value = "Show artworks on sale";
      onSaleButton.setAttribute( "onClick", "javascript: InvestorApp.showArtworksOnSale();" ); // fare il semplice onclick non funziona
      articles.hide();
      loader.hide();
      content.show();

    },

    // simile a showArtworksToAuthenticate di AuthApp ma sufficientemente diverso da giustificare un'altro metodo
    showAuthRequests: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworks = $("#artworksToAuthenticate");
      var artworksList = $("#toAuthenticateList");
      
      artworksList.empty();
      var count = await this.authInstance.countToAuthenticate();
      for(var i=0; i<count; i++){
        var artwork = await this.authInstance.toAuth(i);
        var hash = artwork[0];
        var proposer = artwork[1];

        if(proposer === this.account) {
          var tr = document.createElement('tr');
          var td = document.createElement('td');
  
          var button1 = document.createElement('button');
          button1.innerText = "Show";
          button1.className = "btn btn-primary";
          button1.onclick = function(){
            MemberApp.retrieve(hash);
          };
          td.appendChild(button1);
          tr.appendChild(td);
  
          var button2 = document.createElement('button');
          button2.innerText = "Show";
          button2.className = "btn btn-primary";
          button2.onclick = function() {
            InvestorApp.showProposals(hash);
            $('#modalProposals').modal('show');
          };
          td = document.createElement('td'); // sovrascrivo il puntatore alla cella precedente
          td.appendChild(button2);
          tr.appendChild(td);

          artworksList.append(tr);
        }

      }

      var authRequestsButton = document.getElementById("authRequestsButton");
      authRequestsButton.setAttribute( "onClick", "javascript: InvestorApp.hideAuthRequests();" );
      authRequestsButton.value = "Hide authentication requests";

      artworks.show();
      loader.hide();
      content.show();
    },

    showProposals: async function(hash) {

      var modalBody = $("#proposalsList");
      modalBody.empty();
      var count = await this.authInstance.countProposals(hash);
      if(count == 0) {
        modalBody.append("No proposals released yet");
        $("#registerArtwork").hide();
        return;
      } else {

        for(i = 0; i<count; i++) {
          modalBody.append("<br><input type='radio' name='proposals' value='" + i + "'>");
          var proposal = await this.authInstance.proposals(hash, i);
          for(j = 0; j<4; j++) {
            modalBody.append(" " + proposal[j] + "; ");
          }
          modalBody.append(proposal[j]);
          modalBody.append("<br>");
          var authentications = await this.authInstance.getAuthentications(hash, i);
          modalBody.append("Conferme: <b>" + authentications.length  + "</b><br><br>");
        }

        $("#registerAuthenticatedArtwork")[0].onclick = function() {
          InvestorApp.registerAuthenticatedArtwork(hash);
        }
    
        $("#registerArtwork").show();
      }

    },

    registerAuthenticatedArtwork: async function(hash) {
      var radiovalue = $("input[name='proposals']:checked").val();
      var authentications = await this.authInstance.getAuthentications(hash, radiovalue);
      var requiredAuth = 1;
      if(authentications.length >= requiredAuth){
        this.authInstance.registerAuthenticatedArtwork(hash, radiovalue);
        $('#modalProposals').modal('hide'); // da gestire con then
//        alert("Artwork correctly registered");
      } else {
        var missing = requiredAuth - authentications.length;
        alert("You need " + missing + " more authentications to register the artwork");
        return;
      }
    },

    askForAuthentication: async function(e) {
      var fileUpload = document.getElementById("imageUpload");

      if(fileUpload.files.length > 0) {
        var file = fileUpload.files[0];
      } else {
        alert("Image of the artwork required");
        return;
      }
  
      var hash = await loadImage(file);
      console.log(hash);
      console.log("Image loaded successfully");

      this.authInstance.askForAuthentication(hash); // da gestire con then
      console.log("Request uploaded succesfully");

      e.preventDefault();

    },

    hideAuthRequests: function() {
      var artworksDiv = $("#artworksToAuthenticate");
      var loader = $("#loader");
      var content = $("#content");
  
      var authRequestsButton = document.getElementById("authRequestsButton");
      authRequestsButton.value = "Show authentication requests";
      authRequestsButton.setAttribute( "onClick", "javascript: InvestorApp.showAuthRequests();" );
      artworksDiv.hide();
      loader.hide();
      content.show();
    },

    buy: async function(){
      var form = document.getElementById("buy");
      var tokenId = form[0].value;
      var price = await this.marketplaceInstance.getPrice(tokenId);
      this.marketplaceInstance.buy(tokenId, {value:price});
    },
  
  };
  
  $(function() {
    $(window).load(function() {
      InvestorApp.init();
    });
  });