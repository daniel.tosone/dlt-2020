BrokerApp = {
    brokerMngInstance: null,
    web3Provider: null,
    contracts: {},
    account: '0x0',
    myIndex: 0,
 
    init: function() {
      return this.initWeb3();
    },
  
    initWeb3: function() {
      // se metamask è attivo, viene visto solo l'indirizzo selezionato (array di un elemento)
      // in futuro sarebbe meglio eliminare metamask ma bisogna predisporre la firma delle transazioni in backend
      if (typeof web3 !== 'undefined') {
        // If a web3 instance is already provided by Meta Mask.
        this.web3Provider = web3.currentProvider;
        web3 = new Web3(web3.currentProvider);
      } else {
        // Specify default instance if no web3 instance provided
        this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        web3 = new Web3(this.web3Provider);
      }
      return this.initContracts();
    },
  
    initContracts: async function() {
  
      await $.getJSON("BrokerMng.json", function(brokerMng) {
        BrokerApp.contracts.BrokerMng = TruffleContract(brokerMng);
        BrokerApp.contracts.BrokerMng.setProvider(BrokerApp.web3Provider);
      });
  
      return this.initFields();
    },
  
    initFields: async function(){
  
      // Load account data
      this.account = await web3.eth.accounts[0]; 
      $("#accountAddress").html("Your account:" + this.account);
  
      // Load contracts data
      this.brokerMngInstance = await this.contracts.BrokerMng.deployed();
      myIndex = await this.brokerMngInstance.addressToIndex(this.account);
      return this.render();
    },
  
    render: function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworks = $("#artworks");
      var soldArtworks = $("#soldArtworks");      
      var artists = $("#artists");
      var authenticators = $("#authenticators");
      var brokers = $("#brokers");
      var investors = $("#investors");      
      
      loader.hide();
      artworks.hide();
      soldArtworks.hide();
      artists.hide();
      authenticators.hide();
      brokers.hide();
      investors.hide();
      content.show();
    },
  
    showSoldArtworks: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworksDiv = $("#soldArtworks");
      var soldArtworksList = $("#soldArtworksList");
  
      soldArtworksList.empty();

      var count = await this.brokerMngInstance.countSoldArtworks(this.account);
      for(var i=0; i<count; i++){
        var artwork = await this.brokerMngInstance.soldArtworks(this.account, i);
        var requester = artwork[0];
        var tokenId = artwork[1];
        var sellingPrice = artwork[2];
        var commission = artwork[3];
        var details = (await MemberApp.tokenInstance.tokenURI(tokenId)).split("; ");
        var tr = document.createElement('tr');
        var th = document.createElement('th');
        th.innerText = tokenId;
        tr.appendChild(th);

        for(var j = 0; j<6; j++){
          var td = document.createElement('td');
          td.innerText = details[j];
          tr.appendChild(td);
        }

        var button = MemberApp.createButton(details[6]); // se non uso forEach i pulsanti puntano tutti alla stessa hash. ASSURDO! Con un metodo extra il problema si risolve

        var td = document.createElement('td');
        td.appendChild(button);
        tr.appendChild(td);

        var td = document.createElement('td');
        td.innerText = requester;
        tr.appendChild(td);

        var td = document.createElement('td');
        if(sellingPrice!=0){
          td.innerText = sellingPrice/1000000000000000000;
        } else {td.innerText = "/"}
        tr.appendChild(td);

        var td = document.createElement('td');
        if(commission!=0){
          td.innerText = commission/1000000000000000000;
        } else {td.innerText = "/"}
        tr.appendChild(td);

        soldArtworksList.append(tr);
  
      }

      var soldButton = document.getElementById("soldButton");
      soldButton.value = "Hide sold artworks";
      soldButton.setAttribute( "onClick", "javascript: BrokerApp.hideSoldArtworks();" );
      artworksDiv.show();
      loader.hide();
      content.show();
    },

    hideSoldArtworks: function() {
  
      var artworksDiv = $("#soldArtworks");
      var loader = $("#loader");
      var content = $("#content");
  
      var soldButton = document.getElementById("soldButton");
      soldButton.value = "Show sold artworks";
      soldButton.setAttribute( "onClick", "javascript: BrokerApp.showSoldArtworks();" ); // fare il semplice onclick non funziona
      artworksDiv.hide();
      loader.hide();
      content.show();
    },

    showEntrustedArtworks: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworksDiv = $("#artworks");
  
      var artworks = await this.brokerMngInstance.getEntrustedArtworks(myIndex);
  
      var artworksList = $("#artworksList");
      artworksList.empty();
      artworks.forEach(async function (id) {
        var details = (await MemberApp.tokenInstance.tokenURI(id)).split("; ");
        var tr = document.createElement('tr');
        var th = document.createElement('th');
        th.innerText = id;
        tr.appendChild(th);
        for(var i = 0; i<6; i++){
          var td = document.createElement('td');
          td.innerText = details[i];
          tr.appendChild(td);
        }

        var button = document.createElement('button');
        button.innerText = "Show";
        button.className = "btn btn-primary";
        button.onclick = function(){
          MemberApp.retrieve(details[6]);
        };
        var td = document.createElement('td');
        td.appendChild(button);
        tr.appendChild(td);

        var td = document.createElement('td');
        td.innerText = await MemberApp.brokerMngInstance.getSellingPercentage(id) + "%";
        tr.appendChild(td);

        var td = document.createElement('td');
        td.innerText = await MemberApp.tokenInstance.ownerOf(id);
        tr.appendChild(td);

        artworksList.append(tr);
      });

      var entrustedButton = document.getElementById("entrustedButton");
      entrustedButton.value = "Hide entrusted artworks";
      entrustedButton.setAttribute( "onClick", "javascript: BrokerApp.hideEntrustedArtworks();" );
      artworksDiv.show();
      loader.hide();
      content.show();
    },

    hideEntrustedArtworks: function() {
  
      var artworksDiv = $("#artworks");
      var loader = $("#loader");
      var content = $("#content");
  
      var entrustedButton = document.getElementById("entrustedButton");
      entrustedButton.value = "Show entrusted artworks";
      entrustedButton.setAttribute( "onClick", "javascript: BrokerApp.showEntrustedArtworks();" ); // fare il semplice onclick non funziona
      artworksDiv.hide();
      loader.hide();
      content.show();
    },

  };
  
  $(function() {
    $(window).load(function() {
      BrokerApp.init();
    });
  });
