var web3Provider;
var account;
var artistMngInstance;
var brokerMngInstance;
var authInstance;


$(function() {
    $(window).load(function() {
        init();
    });
});

async function init() {

    // initWeb3
    web3Provider = web3.currentProvider;
    web3 = new Web3(web3.currentProvider);

    // initContracts
    await $.getJSON("ArtistMng.json", function(artistMng) {
        ArtistMng = TruffleContract(artistMng);
        ArtistMng.setProvider(web3Provider);
    });
    await $.getJSON("Authentication.json", function(authentication) {
        Authentication = TruffleContract(authentication);
        Authentication.setProvider(web3Provider);
    });
    await $.getJSON("BrokerMng.json", function(brokerMng) {
        BrokerMng = TruffleContract(brokerMng);
        BrokerMng.setProvider(web3Provider);
    });
    await $.getJSON("Marketplace.json", function(marketplace) {
        Marketplace = TruffleContract(marketplace);
        Marketplace.setProvider(web3Provider);
    });

    // initFields
    account = await web3.eth.accounts[0];
    artistMngInstance = await ArtistMng.deployed();
    brokerMngInstance = await BrokerMng.deployed();
    authInstance = await Authentication.deployed();
    marketplaceInstance = await Marketplace.deployed();

    associate();
}



async function associate() {
    if(account === await artistMngInstance.owner()) {
        window.location.href = "administrator.html";
    }
    else if(await artistMngInstance.isArtist(account)) {
        window.location.href = "artist.html";
    }
    else if(await authInstance.isAuthenticator(account)) {
        window.location.href = "authenticator.html";
    } 
    else if(await brokerMngInstance.isBroker(account)) {
        window.location.href = "broker.html";
    }
    else if(await marketplaceInstance.isInvestor(account)){
        window.location.href = "investor.html";
    }

}