'use strict'

let node
let fileSize = 0
const { Buffer } = Ipfs

function start(){
//  node = new Ipfs()
  node = window.IpfsHttpClient('localhost', '5001');
}

async function loadImage(file) {

  await node.ready // aspetta la terminazione della chiamata asincrona ipfs.create()

  function readFileContents (file) {
    return new Promise((resolve) => {
      const reader = new window.FileReader()
      reader.onload = (event) => resolve(event.target.result)
      reader.readAsArrayBuffer(file)
    })
  }
  
  var buffer = await readFileContents(file);
  fileSize = file.size

  var results = await node.add({
    path: file.name,
    content: Buffer.from(buffer)
  })

  var hash = results[0].hash
  return hash
}

start()