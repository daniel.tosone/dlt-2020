MemberApp = {
    tokenInstance: null,
    artistMngInstance: null,
    brokerMngInstance: null,
    marketplaceInstance: null,
    authInstance: null,
    web3Provider: null,
    contracts: {},
    account: '0x0',
  
    init: function() {
      return this.initWeb3();
    },
  
    initWeb3: function() {
      if (typeof web3 !== 'undefined') {
        // Se viene già fornita un'istanza di web3 da Metamask, allora viene selezionata
        this.web3Provider = web3.currentProvider;
        web3 = new Web3(web3.currentProvider);
      } else {
        // Altrimenti si usa un'istanza di default (blockchain locale)
        this.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        web3 = new Web3(this.web3Provider);
      }
      return this.initContracts();
    },
  
    initContracts: async function() {
      await $.getJSON("Token.json", function(token) {
        // Istanzia un nuovo truffle-contract a partire dall'artefatto
        MemberApp.contracts.Token = TruffleContract(token);
        // Connette il provider per interagire con il contratto
        MemberApp.contracts.Token.setProvider(MemberApp.web3Provider);
      });
  
      await $.getJSON("ArtistMng.json", function(artistMng) {
        MemberApp.contracts.ArtistMng = TruffleContract(artistMng);
        MemberApp.contracts.ArtistMng.setProvider(MemberApp.web3Provider);
      });
  
      await $.getJSON("BrokerMng.json", function(brokerMng) {
        MemberApp.contracts.BrokerMng = TruffleContract(brokerMng);
        MemberApp.contracts.BrokerMng.setProvider(MemberApp.web3Provider);
      });

      await $.getJSON("Marketplace.json", function(marketplace) {
        MemberApp.contracts.Marketplace = TruffleContract(marketplace);
        MemberApp.contracts.Marketplace.setProvider(MemberApp.web3Provider);
      });

      await $.getJSON("Authentication.json", function(authentication) {
        MemberApp.contracts.Authentication = TruffleContract(authentication);
        MemberApp.contracts.Authentication.setProvider(MemberApp.web3Provider);
      });

      return this.initFields();
    },
  
    initFields: async function(){
  
      // Load account data
      this.account = await web3.eth.accounts[0]; 
      $("#accountAddress").html("Your account:" + this.account);
  
      // Load contracts data
      this.tokenInstance = await this.contracts.Token.deployed();
      this.artistMngInstance = await this.contracts.ArtistMng.deployed();
      this.brokerMngInstance = await this.contracts.BrokerMng.deployed();
      this.marketplaceInstance = await this.contracts.Marketplace.deployed();
      this.authInstance = await this.contracts.Authentication.deployed();
  
    },
  
    showOwnedArtworks: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworksDiv = $("#artworks");
  
      var th = document.createElement("th");
      var tr = $(".table")[0].children[0].children[0] // recupero la prima riga della tabella delle opere possedute
      th.innerText = "Purchase price(Ether)"; // aggiungo il prezzo d'acquisto ai campi
      tr.appendChild(th);
     
      artworks = await this.tokenInstance.getTokensOfOwner(this.account);
      this.displayArtworks(artworks, true);
  
      document.getElementById("titleArtworks").innerText = "Artworks you own";
      var ownedButton = document.getElementById("ownedButton");
      ownedButton.value = "Hide owned artworks";
      ownedButton.setAttribute( "onClick", "javascript: MemberApp.hideOwnedArtworks();" );
      artworksDiv.show();
      loader.hide();
      content.show();
    },
  
    hideOwnedArtworks: function() {
  
      var artworksDiv = $("#artworks");
      var loader = $("#loader");
      var content = $("#content");
  
      var ownedButton = document.getElementById("ownedButton");
      ownedButton.value = "Show owned artworks";
      ownedButton.setAttribute( "onClick", "javascript: MemberApp.showOwnedArtworks();" ); // fare il semplice onclick non funziona
      
      var tr = $(".table")[0].children[0].children[0];
      tr.removeChild(tr.lastElementChild);
      
      artworksDiv.hide();
      loader.hide();
      content.show();
    },
  
    showProducedArtworks: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var artworksDiv = $("#artworks");
  
      var account = document.getElementById("showProducedArtworks")[0].value;
      var index = await this.artistMngInstance.addressToIndex(account);
      var artworks = await this.artistMngInstance.getProducedArtworks(index);
      this.displayArtworks(artworks);

      document.getElementById("titleArtworks").innerText = "Artworks produced by " + account;

      var producedButton = document.getElementById("showProducedArtworks")[1];

      producedButton.value = "Hide produced artworks";
      producedButton.setAttribute( "onClick", "javascript: MemberApp.hideProducedArtworks();" );  

      artworksDiv.show();
      loader.hide();
      content.show();
    },
  
    hideProducedArtworks: function() {
  
      var artworksDiv = $("#artworks");
      var loader = $("#loader");
      var content = $("#content");
  
      var producedButton = document.getElementById("showProducedArtworks")[1];
      producedButton.value = "Show produced artworks";
      producedButton.setAttribute( "onClick", "javascript: MemberApp.showProducedArtworks();" ); // fare il semplice onclick non funziona
      artworksDiv.hide();
      loader.hide();
      content.show();
    },
    
    displayArtworks: async function(artworks, owned = false) {
      var artworksList = $("#artworksList");
      artworksList.empty();
      artworks.forEach(async function (id) {
        var details = (await MemberApp.tokenInstance.tokenURI(id)).split("; ");
        var tr = document.createElement('tr');
        var th = document.createElement('th');
        th.innerText = id;
        tr.appendChild(th);
        for(var i = 0; i<6; i++){
          var td = document.createElement('td');
          td.innerText = details[i];
          tr.appendChild(td);
        }
        var button = document.createElement('button');
        button.innerText = "Show";
        button.className = "btn btn-primary";
        button.onclick = function(){
          MemberApp.retrieve(details[6]);
        };
        tr.appendChild(button);

        if(owned) { // solo per i quadri che posseggo visualizzo il prezzo d'acquisto
          var td = document.createElement('td');
          var purchasePrice = await MemberApp.marketplaceInstance.lastSellingPrice(id);
          if(purchasePrice == 0) {
            td.innerText = "/";  
          } else {
            td.innerText = purchasePrice/1000000000000000000;
          }
          tr.appendChild(td);          
        }
        artworksList.append(tr);
      });
    },

    retrieve: function(hash){
      var img = document.getElementById('retrievedImg');
      img.setAttribute('src', "http://127.0.0.1:8080/ipfs/" + hash);
      $('#modalImg').modal('show');
    },

    sellOnMarketplace: function() {
        var form = document.getElementById("sellOnMarketplace");
        var tokenId = form[0].value;
        var price = form[1].value;
        this.marketplaceInstance.startOpenSale(tokenId, web3.toWei(price, "ether")); // da gestire con un then
//        console.log("Artwork correctly put on sale on Marketplace. You will be notified when someone buys this artwork and withdrawal is available");
    },

    sellTo: function() {
        var form = document.getElementById("sellTo");
        var tokenId = form[0].value;
        var price = form[1].value;
        var to = form[2].value;
        var private = form[3].checked;
        if(private) {
          this.marketplaceInstance.sellPrivately(tokenId, to); // da gestire con un then
//          console.log("Transfer concluded. Payment should be done outside Ethereum");
        } else {
          this.marketplaceInstance.startClosedSale(tokenId, web3.toWei(price, "ether"), to); // da gestire con un then
//          console.log("Sale to designed address started");
        }
    },

    entrust: function(){
        var form = document.getElementById("entrust");
        var tokenId = form[0].value;
        var to = form[1].value;
        var percentage = form[2].value;
        this.brokerMngInstance.entrustArtwork(to, tokenId, percentage); // da gestire con un then
//        console.log("Approvance for the selected token succesfull");
    },

    createButton: function(hash) {
      var button = document.createElement('button');
      button.innerText = "Show";
      button.className = "btn btn-primary";
      button.onclick = function(){
        MemberApp.retrieve(hash);
      };
      return button;
    },

    withdraw: function(address) {
      this.marketplaceInstance.withdraw(address); // da gestire con then
//      console.log("Resale right funds released");
  },

//--------------------SHOW/HIDE--------------------------------------------------------------------------

    showArtists: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var artists = $("#artists");
      var artistsList = $("#artistsList");

      artistsList.empty();
      for(var i=0; i<await this.artistMngInstance.countArtists(); i++){
        var artist = await this.artistMngInstance.artists(i);
        var address = artist[0];
        var name = artist[1];
        var email = artist[2];
        artistsList.append("<tr class='text-left'><td>" + address + "</td><td>" + name + "</td><td>" + email + "</td></tr>");
      }

      var artistsButton = document.getElementById("artistsButton");
      artistsButton.setAttribute( "onClick", "javascript: MemberApp.hideArtists();" );
      artistsButton.value = "Hide artists";

      artists.show();
      loader.hide();
      content.show();
    },

    hideArtists: function() {

      var artists = $("#artists");
      var loader = $("#loader");
      var content = $("#content");

      var artistsButton = document.getElementById("artistsButton");
      artistsButton.value = "Show artists";
      artistsButton.setAttribute( "onClick", "javascript: MemberApp.showArtists();" ); // fare il semplice onclick non funziona
      artists.hide();
      loader.hide();
      content.show();
    },

    showBrokers: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var brokers = $("#brokers");
      var brokersList = $("#brokersList");

      brokersList.empty();
      for(var i=0; i<await this.brokerMngInstance.countBrokers(); i++){
        var broker = await this.brokerMngInstance.brokers(i);
        var address = broker[0];
        var name = broker[1];
        var email = broker[2];
        brokersList.append("<tr class='text-left'><td>" + address + "</td><td>" + name + "</td><td>" + email + "</td></tr>");
      }

      var brokersButton = document.getElementById("brokersButton");
      brokersButton.setAttribute( "onClick", "javascript: MemberApp.hideBrokers();" );
      brokersButton.value = "Hide brokers";

      brokers.show();
      loader.hide();
      content.show();
    },

    hideBrokers: function() {

      var brokers = $("#brokers");
      var loader = $("#loader");
      var content = $("#content");

      var brokersButton = document.getElementById("brokersButton");
      brokersButton.value = "Show brokers";
      brokersButton.setAttribute( "onClick", "javascript: MemberApp.showBrokers();" ); // fare il semplice onclick non funziona
      brokers.hide();
      loader.hide();
      content.show();
    },

    showAuthenticators: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var authenticators = $("#authenticators");
      var authenticatorsList = $("#authenticatorsList");

      authenticatorsList.empty();
      for(var i=0; i<await this.authInstance.countAuthenticators(); i++){
        var authenticator = await this.authInstance.authenticators(i);
        var address = authenticator[0];
        var name = authenticator[1];
        var email = authenticator[2]; // gli altri campi vengono annullati perchè non restituibili
        authenticatorsList.append("<tr class='text-left'><td>" + address + "</td><td>" + name + "</td><td>" + email + "</td></tr>");
      }

      var authenticatorsButton = document.getElementById("authenticatorsButton");
      authenticatorsButton.setAttribute( "onClick", "javascript: MemberApp.hideAuthenticators();" );
      authenticatorsButton.value = "Hide authenticators";

      authenticators.show();
      loader.hide();
      content.show();
    },

    hideAuthenticators: function() {

      var authenticators = $("#authenticators");
      var loader = $("#loader");
      var content = $("#content");

      var authenticatorsButton = document.getElementById("authenticatorsButton");
      authenticatorsButton.value = "Show authenticators";
      authenticatorsButton.setAttribute( "onClick", "javascript: MemberApp.showAuthenticators();" ); // fare il semplice onclick non funziona
      authenticators.hide();
      loader.hide();
      content.show();
    },

    showInvestors: async function() {
      var loader = $("#loader");
      var content = $("#content");
      var investors = $("#investors");
      var investorsList = $("#investorsList");

      investorsList.empty();
      for(var i=0; i<await this.marketplaceInstance.countInvestors(); i++){
        var investor = await this.marketplaceInstance.investors(i);
        var address = investor[0];
        var professional = investor[1];
        var email = investor[2];
        investorsList.append("<tr class='text-left'><td>" + address + "</td><td>" + professional + "</td><td>" + email + "</td></tr>");
      }

      var investorsButton = document.getElementById("investorsButton");
      investorsButton.setAttribute( "onClick", "javascript: MemberApp.hideInvestors();" );
      investorsButton.value = "Hide investors";

      investors.show();
      loader.hide();
      content.show();
    },

    hideInvestors: function() {

      var investors = $("#investors");
      var loader = $("#loader");
      var content = $("#content");

      var investorsButton = document.getElementById("investorsButton");
      investorsButton.value = "Show investors";
      investorsButton.setAttribute( "onClick", "javascript: MemberApp.showInvestors();" ); // fare il semplice onclick non funziona
      investors.hide();
      loader.hide();
      content.show();
    },

//-------------------------------------------------------------------------------------------------------

  };
  
  $(function() {
    $(window).load(function() {
      MemberApp.init();
    });
  });