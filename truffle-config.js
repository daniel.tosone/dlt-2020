/**
 * More information about configuration can be found at:
 *
 * truffleframework.com/docs/advanced/configuration
 *
 */

const HDWalletProvider = require('@truffle/hdwallet-provider');
const fs = require('fs');
const MNEMONIC = fs.readFileSync(".secret").toString().trim();

module.exports = {
  networks: {
    // Useful for testing. The `development` name is special - truffle uses it by default
    development: {
      host: "127.0.0.1",     // Localhost (default: none)
      port: 7545,            
      network_id: "*",       // Any network
      gasPrice: 100000000,   // 0,1 GWei // GasPrice for deployments can be low
    },

    ropsten: {
      provider: function(){
        return new HDWalletProvider(MNEMONIC, "https://ropsten.infura.io/v3/346ec1bfcde3499cb307872ad635b627")
      }, 
      network_id: 3,          // Ropsten's id
      gasPrice: 30000000000,  // 30 GWei
    },

  },

  compilers: {
    solc: {
      version: "0.5.8",
    }
  }
}