var Token = artifacts.require("Token.sol");
var ArtistMng = artifacts.require("ArtistMng.sol");
var Marketplace = artifacts.require("Marketplace.sol");
var Authentication = artifacts.require("Authentication.sol");
var BrokerMng = artifacts.require("BrokerMng.sol");

module.exports = function(deployer) {
    deployer.deploy(Token);
    deployer.deploy(ArtistMng);
    deployer.deploy(Marketplace);
    deployer.deploy(Authentication);
    deployer.deploy(BrokerMng)

    // Additional contracts can be deployed here
};